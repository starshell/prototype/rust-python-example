# rust-python-example

An example of extending rust with python [1][1].

## Note

To import the library directly as if it was a Python module it must either be in the same folder or in the `PYTHONPATH`.

```sh
PYTHONPATH="$PYTHONPATH:$HOME/path/to/my/worksapce/rust-python-example/target/release/librustypython.so"
```

---

[1]: https://developers.redhat.com/blog/2017/11/16/speed-python-using-rust/
